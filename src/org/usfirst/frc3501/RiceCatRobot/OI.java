package org.usfirst.frc3501.RiceCatRobot;

import org.usfirst.frc3501.RiceCatRobot.commands.CloseClaw;
import org.usfirst.frc3501.RiceCatRobot.commands.IncrementSpeed;
import org.usfirst.frc3501.RiceCatRobot.commands.MoveArmDownLevel;
import org.usfirst.frc3501.RiceCatRobot.commands.MoveArmUpLevel;
import org.usfirst.frc3501.RiceCatRobot.commands.OpenClaw;
import org.usfirst.frc3501.RiceCatRobot.commands.ToggleClaw;
import org.usfirst.frc3501.RiceCatRobot.commands.ToggleCompressor;
import org.usfirst.frc3501.RiceCatRobot.commands.TurnDegrees;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
	// One type of button is a joystick button which is any button on a
	// joystick.
	// You create one by telling it which joystick it's on and which button
	// number it is.
	// Joystick stick = new Joystick(port);
	// Button button = new JoystickButton(stick, buttonNumber);
	// There are a few additional built in buttons you can use. Additionally,
	// by subclassing Button you can create custom triggers and bind those to
	// commands the same as any other Button.

	// // TRIGGERING COMMANDS WITH BUTTONS
	// Once you have a button, it's trivial to bind it to a button in one of
	// three ways:

	// Start the command when the button is pressed and let it run the command
	// until it is finished as determined by it's isFinished method.
	// button.whenPressed(new ExampleCommand());

	// Run the command while the button is being held down and interrupt it once
	// the button is released.
	// button.whileHeld(new ExampleCommand());

	// Start the command when the button is released and let it run the command
	// until it is finished as determined by it's isFinished method.
	// button.whenReleased(new ExampleCommand());
	public Joystick leftJoystick;
	public Joystick rightJoystick;
	public JoystickButton trigger;
	public JoystickButton toggleCompressor;
	public JoystickButton toggleClaw;
	final int level = 0;

	// TODO: move constants to config file and read from there

	// joystick ports
	private final static int LEFT_JOYSTICK_PORT = 0, RIGHT_JOYSTICK_PORT = 1;
	private final static int TRIGGER_PORT = 1, TOGGLE_PORT = 2,
	                         TOGGLE_COMPRESSOR_PORT = 11, POV_PORT = 0;
	// POV constants for angle of POV
	public final static int POV_UP = 0, POV_DOWN = 180, POV_NOT_PRESSED = -1;

	public OI() {
		System.out.println("OI is open");
		leftJoystick = new Joystick(LEFT_JOYSTICK_PORT);
		rightJoystick = new Joystick(RIGHT_JOYSTICK_PORT);

		trigger = new JoystickButton(rightJoystick, TRIGGER_PORT);
	
		toggleClaw = new JoystickButton(rightJoystick, TOGGLE_PORT);
		toggleClaw.whenPressed(new ToggleClaw());

		toggleCompressor = new JoystickButton(rightJoystick, TOGGLE_COMPRESSOR_PORT);
		toggleCompressor.whenPressed(new ToggleCompressor());
		// trigger needs to be added

		// SmartDashboard Buttons
		SmartDashboard.putData("Turn",
		                       new TurnDegrees(RobotMap.Direction.LEFT,
		                                       90));
		SmartDashboard.putData("OpenClaw", new OpenClaw());
		SmartDashboard.putData("CloseClaw", new CloseClaw());
		SmartDashboard.putData("MoveArmUpLevel", new MoveArmUpLevel());
		SmartDashboard.putData("MoveArmDownLevel", new MoveArmDownLevel());
	}

	public int getPOVValue() {
		return rightJoystick.getPOV(POV_PORT);
	}

	public void doTriggerAction() {
		if (!Robot.claw.toggleOn) {
			if (rightJoystick.getRawButton(TRIGGER_PORT)) {
				if (Robot.claw.isOpen) {
					new CloseClaw().start();
				}
			} else {
				if (!Robot.claw.isOpen) {
					new OpenClaw().start();
				}
			}
		}
	}

	public void toggleOn() {
		if (rightJoystick.getRawButton(TOGGLE_PORT)) {
			Robot.claw.toggleOn = !Robot.claw.toggleOn;
		}

		if (Robot.claw.toggleOn && Robot.claw.isOpen()) {
			new CloseClaw().start();
		}
	}
}