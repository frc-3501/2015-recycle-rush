package org.usfirst.frc3501.RiceCatRobot;

import org.usfirst.frc3501.RiceCatRobot.commands.BrickBot;
import org.usfirst.frc3501.RiceCatRobot.commands.OneTotePickUp;
import org.usfirst.frc3501.RiceCatRobot.subsystems.Arm;
import org.usfirst.frc3501.RiceCatRobot.subsystems.Claw;
import org.usfirst.frc3501.RiceCatRobot.subsystems.DriveTrain;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot {
	static Timer timer;
	public static OI oi;
	public static DriveTrain driveTrain;
	public static Arm arm;
	public static Claw claw;
	// debug constants
	public int countTeleopPeriodic;
	public int countAutonPeriodic;

	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	public void robotInit() {
		RobotMap.init();
		timer = new Timer();
		driveTrain = new DriveTrain();
		arm = new Arm();
		claw = new Claw();
		// OI must be constructed after subsystems. If the OI creates Commands
		// (which it very likely will), subsystems are not guaranteed to be
		// constructed yet. Thus, their requires() statements may grab null
		// pointers. Bad news. Don't move it.
		oi = new OI();
		countTeleopPeriodic = 0;
		driveTrain.leftEncoder.setDistancePerPulse(RobotMap.DISTANCE_PER_PULSE);
		driveTrain.rightEncoder
				.setDistancePerPulse(RobotMap.DISTANCE_PER_PULSE);
	}

	/**
	 * This function is called when the disabled button is hit. You can use it
	 * to reset subsystems before shutting down.
	 */
	public void disabledInit() {

	}

	public void disabledPeriodic() {
		Scheduler.getInstance().run();
	}

	public void autonomousInit() {
		// The compressor vibrates a lot. Autonomous will be more accurate
		// if the compressor is off
		RobotMap.compressor.start();
		Scheduler.getInstance().add(new BrickBot(true));
	}

	/**
	 * This function is called periodically during autonomous
	 */
	public void autonomousPeriodic() {
		// TODO uncomment when encoders are working.
		// RobotMap.debug(1,
		// "Left: "
		// + RobotMap.driveTrainLeftEncoder.getDistance());
		// RobotMap.debug(1,
		// "\nRight: "
		// + RobotMap.driveTrainRightEncoder.getDistance());
		Scheduler.getInstance().run();
		if (countAutonPeriodic++ % 40 == 0) {
			RobotMap.debug(1,
					"Left: " + RobotMap.driveTrainLeftEncoder.getDistance());
			RobotMap.debug(1,
					"\nRight: " + RobotMap.driveTrainRightEncoder.getDistance());
		}

	}

	public void teleopInit() {
		RobotMap.compressor.start();
		countTeleopPeriodic = 0;
	}

	/**
	 * This function is called periodically during operator control
	 */
	public void teleopPeriodic() {
		//TODO debug statement, remove later
		 DriverStation.reportError("Left: "
		 + RobotMap.driveTrainLeftEncoder.getDistance()
		 + "\nRight: "
		 + RobotMap.driveTrainRightEncoder.getDistance(), false);
		
		countTeleopPeriodic++;
		Scheduler.getInstance().run();
		driveTrain.arcadeDrive(Robot.oi.rightJoystick.getY(),
				Robot.oi.rightJoystick.getTwist());
		oi.doTriggerAction();
		if (countTeleopPeriodic % 40 == 0) {
			System.out.println("Left: "
					+ Robot.driveTrain.getLeftEncoderDistance());
			System.out.println("Right: "
					+ Robot.driveTrain.getRightEncoderDistance());
		}
		boolean anyButtonPressed = oi.leftJoystick.getRawButton(6)
				|| oi.leftJoystick.getRawButton(7)
				|| oi.leftJoystick.getRawButton(10)
				|| oi.leftJoystick.getRawButton(11)
				|| (oi.leftJoystick.getRawButton(8) || oi.leftJoystick
						.getRawButton(9));

		if (anyButtonPressed) {
			if (oi.leftJoystick.getRawButton(8)) {
				arm.setArmSpeeds(0.3);
			} else if (oi.leftJoystick.getRawButton(9)) {
				arm.setArmSpeeds(-0.3);
			} else if (oi.leftJoystick.getRawButton(6)) {
				RobotMap.armLeft.set(0.3);
			} else if (oi.leftJoystick.getRawButton(7)) {
				RobotMap.armLeft.set(-0.3);
			} else if (oi.leftJoystick.getRawButton(11)) {
				RobotMap.armRight.set(-0.3);
			} else if (oi.leftJoystick.getRawButton(10)) {
				RobotMap.armRight.set(0.3);
			}
		} else {
			if (Math.abs(oi.leftJoystick.getY()) < 0.1) {
				arm.fineTuneControl(0);

			} else {
				arm.fineTuneControl(oi.leftJoystick.getY());
			}

		}
	}

	/**
	 * This function is called periodically during test mode
	 */
	public void testPeriodic() {
		LiveWindow.run();
	}
}