package org.usfirst.frc3501.RiceCatRobot;

import edu.wpi.first.wpilibj.CANJaguar;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.CounterBase.EncodingType;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDSource.PIDSourceParameter;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
	public static final int debugLevel = 100;

	// Joysticks
	public static final int X_AXIS = 0, Y_AXIS = 1, TWIST = 2;

	// Drivetrain
	public static boolean driveWithPID;
	public static RobotDrive robotDrive;
	public static CANJaguar driveTrainFrontLeft, driveTrainFrontRight,
	       driveTrainRearLeft, driveTrainRearRight;
	public static final int DRIVETRAIN_FRONT_LEFT_PORT = 4,
	                        DRIVETRAIN_FRONT_RIGHT_PORT = 5,
	                        DRIVETRAIN_REAR_LEFT_PORT = 3,
	                        DRIVETRAIN_REAR_RIGHT_PORT = 6;
	public static Encoder driveTrainLeftEncoder, driveTrainRightEncoder;
	public static final int DRIVETRAIN_LEFT_ENCODER_CHANNEL_A = 4,
	                        DRIVETRAIN_LEFT_ENCODER_CHANNEL_B = 3,
	                        DRIVETRAIN_RIGHT_ENCODER_CHANNEL_A = 2,
	                        DRIVETRAIN_RIGHT_ENCODER_CHANNEL_B = 1;
	public static final double DISTANCE_FROM_CENTER_TO_WHEELS = 15.5;
	public static PIDController frontLeft, frontRight, rearLeft, rearRight;
	public static double P = 0.9, I = 0, D = 0;
	public static final double TOLERABLE_CHANGE_IN_JOYSTICK_PWM = 0.3;
	public static final double TOLERABLE_CHANGE_IN_SPEED = 6; // inches / second
	public static final double DAMPENING_FACTOR = 0.95;
	public static final double IDEAL_MAX_SPEED = 144;
	/*
	 * DISTANCE_PER_PULSE calculates the distance a wheel goes every time the
	 * encoder counts a pulse: Calculated by finding ratio between the large
	 * sprocket on the gear 102 * pulse This calculation is the ratio between
	 * the radius of the gear on the box and the small sprocket on the wheels.
	 * Multiplies it by the 103 * motor and the radius of the gear on the wheel
	 * multiplied by the circumference of the wheel and divide by the pulses per
	 * revolution of the 104 * circumference of the wheel The above value is
	 * then divided by 256 which encoder.
	 */
	//Autonomous constants
	public static final double SECONDS_TO_AUTO_ZONE = 2.1;
	public static final double SECONDS_TO_NEXT_TOTE = 3;
	public static final double SECONDS_TO_TURN_90 = 2;
	public static final double INCHES_TO_AUTO_ZONE = 60;
	public static final double INCHES_FORWARD_TO_TOTE = 6;
	public static final double SECONDS_TO_TOTE = 0.5;
	public static final double SECONDS_UP_ONE_LEVEL = 1;
	public static final double SECONDS_DOWN_ONE_LEVEL = 0.3;


	/**
	 * Calculates the distance the wheels goes every time the encoder counts a
	 * pulse:
	 *
	 * This calculation is the ratio between the radius of the gear on the box
	 * and the small sprocket on the wheels, multiplied by the circumference of
	 * the wheel, and divided by the pulses per revolution of the encoder (256).
	 */
	public static final double DISTANCE_PER_PULSE =
	    ((3.66 / 5.14) * 6 * Math.PI) / 256;
	// Arm
	public static CANJaguar armLeft, armRight;
	public PIDController armLeftController, armRightController;
	public static final int ARM_LEFT_PORT = 2, ARM_RIGHT_PORT = 7;
	public static double[] armLevelLidarValues;
	public static final int ARM_HIGHEST_LEVEL = 7, ARM_LOWEST_LEVEL = 0;
	public static final double ARM_HIGH_SPEED = 0.5, ARM_LOW_SPEED = 0.5;
	// TODO find value for threshold to be used in arm methods
	public static final double ARM_LIDAR_THRESHOLD = 5;

	// Claw
	public static DoubleSolenoid clawSolenoid;
	public final static Value open = DoubleSolenoid.Value.kForward,
	                          close = DoubleSolenoid.Value.kReverse;
	public static final int CLAW_FORWARD_PORT = 1, CLAW_REVERSE_PORT = 2;

	// Compressor
	public static final int COMPRESSOR_RELAY_CHANNEL = 7,
	                        PRESSURE_SWITCH_CHANNEL = 10;

	// Used in speedAtDist
	private static final double CURVATURE = 10;

	// Compressor
	public static Compressor compressor;

	public static void init() {
		driveWithPID = true;

		driveTrainFrontLeft = new CANJaguar(DRIVETRAIN_FRONT_LEFT_PORT);
		driveTrainFrontRight = new CANJaguar(DRIVETRAIN_FRONT_RIGHT_PORT);
		driveTrainRearLeft = new CANJaguar(DRIVETRAIN_REAR_LEFT_PORT);
		driveTrainRearRight = new CANJaguar(DRIVETRAIN_REAR_RIGHT_PORT);
		driveTrainLeftEncoder = new Encoder(DRIVETRAIN_LEFT_ENCODER_CHANNEL_A,
		                                    DRIVETRAIN_LEFT_ENCODER_CHANNEL_B,
		                                    false,
		                                    EncodingType.k4X);
		LiveWindow
		.addSensor("DriveTrain", "leftEncoder", driveTrainLeftEncoder);
		driveTrainLeftEncoder.setDistancePerPulse(DISTANCE_PER_PULSE);
		driveTrainLeftEncoder.setPIDSourceParameter(PIDSourceParameter.kRate);

		driveTrainRightEncoder = new Encoder(DRIVETRAIN_RIGHT_ENCODER_CHANNEL_A,
		                                     DRIVETRAIN_RIGHT_ENCODER_CHANNEL_B,
		                                     false,
		                                     EncodingType.k4X);
		LiveWindow.addSensor("DriveTrain", "rightEncoder", driveTrainRightEncoder);
		driveTrainRightEncoder.setDistancePerPulse(DISTANCE_PER_PULSE);
		driveTrainRightEncoder.setPIDSourceParameter(PIDSourceParameter.kRate);

		armLeft = new CANJaguar(ARM_LEFT_PORT);
		armRight = new CANJaguar(ARM_RIGHT_PORT);
		armLevelLidarValues = new double[ARM_HIGHEST_LEVEL + 1];

		clawSolenoid = new DoubleSolenoid(0, 0, 1);
		LiveWindow.addActuator("Claw", "solenoid", clawSolenoid);
		compressor = new Compressor(0);
	}

	public static double getTargetSpeed(double joystickValue) {
		return Math.abs(joystickValue) * joystickValue * IDEAL_MAX_SPEED;
	}

	public static double toPWM(double speed) {
		return speed / IDEAL_MAX_SPEED;
	}

	public static double toSpeed(double PWMValue) {
		return PWMValue * IDEAL_MAX_SPEED;
	}

	public static void debug(int lvl, String msg) {
		if (lvl <= debugLevel) {
			System.out.println(msg);
		}
	}

	public static enum Direction {
		LEFT, RIGHT, DOWN, UP, FORWARD, BACKWARD;
	}

	/**
	 * Used to smoothly scale motor speed from 0 to 1 between initial and target
	 * positions. Calculated via modified normal distribution to make motor
	 * speed up from 0 to 1 and slow down back to 0 as it approaches its
	 * destination.
	 *
	 * @param dist
	 *            current position, from 0 to target
	 * @param target
	 *            target position
	 *
	 * @return a motor speed between 0 and 1
	 */
	public static double speedAtDist(double dist, double target) {
		double a = CURVATURE;
		double b = target / 2;
		double p = Math.exp((b * b) / (2 * a * a));
		double r = Math.exp(((dist - b) * (dist - b)) / (2 * a * a));
		double q = 1 / (p - 1);
		return ((1 / (r - (r / p))) - q);
	}
}