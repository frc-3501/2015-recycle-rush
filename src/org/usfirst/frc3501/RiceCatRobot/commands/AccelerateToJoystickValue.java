package org.usfirst.frc3501.RiceCatRobot.commands;

import org.usfirst.frc3501.RiceCatRobot.Robot;
import org.usfirst.frc3501.RiceCatRobot.RobotMap;
import edu.wpi.first.wpilibj.command.Command;

/**
 * Makes an accelerate to a certain speed in JoystickValues(-1 to 1)
 * very smooth. Doesn't automatically set the speed.
 * Resolves the problem of going from full speed forward(1) to full speed
 * backward(-1)
 */
public class AccelerateToJoystickValue extends Command {
	private double desiredRight;
	private double desiredLeft;
	private boolean isRightSlower;
	private boolean isLeftSlower;
	private double curLeftSpeed;
	private double curRightSpeed;

	// debug constants
	private int count;
	/*
	 * @param right and left speeds need to be between -1 and 1
	 */
	public AccelerateToJoystickValue(double rightSpeed, double leftSpeed) {
		requires(Robot.driveTrain);
		this.desiredRight = rightSpeed;
		this.desiredLeft = leftSpeed;
		this.isRightSlower = false;
		this.isLeftSlower = false;
		this.count = 0;
	}

	protected void initialize() {
		// TODO Auto-generated method stub
		curRightSpeed = RobotMap.toPWM(Robot.driveTrain.rightEncoder.getRate());
		curLeftSpeed = RobotMap.toPWM(Robot.driveTrain.leftEncoder.getRate());
		isLeftSlower = curLeftSpeed < desiredLeft;
		isRightSlower = curRightSpeed < desiredRight;
		RobotMap.debug(1, ("accelerateToSpeed-initialize()\n"
		                   + "\tDesired Right Speed: " + desiredRight
		                   + "\n\tDesired Left Speed: " + desiredLeft
		                   + "\n\tCurrent Drive Left Speed: " + curLeftSpeed
		                   + "\n\tCurrent drive right speed" + curRightSpeed));
	}

	protected void execute() {
		if (Math.abs(desiredLeft - curLeftSpeed) >
		        RobotMap.TOLERABLE_CHANGE_IN_JOYSTICK_PWM) {
			// set the lastLeft to this value here
			// (sets it to a calibration ratio; false average)
			curLeftSpeed = ((desiredLeft + curLeftSpeed * 5) / 6);
		} else {
			// otherwise set lastLeft to the leftStick value
			curLeftSpeed = desiredLeft;
		}
		// do the same for the rightStick
		if (Math.abs(desiredRight - curRightSpeed) >
		        RobotMap.TOLERABLE_CHANGE_IN_JOYSTICK_PWM) {
			curRightSpeed = ((desiredRight + curRightSpeed * 5) / 6);
		} else {
			curRightSpeed = desiredRight;
		}
		if (count % 5 == 0) {
			RobotMap.debug(2, ("accelerationToSpeed-Execute()\n"
			                   + "\tCurrent Left Speed: " + curRightSpeed
			                   + "\n\tCurrent Right Speed: " + curRightSpeed));
		}
		double dampeningFactor = RobotMap.DAMPENING_FACTOR;
		RobotMap.driveTrainFrontLeft.set(curLeftSpeed * dampeningFactor);
		RobotMap.driveTrainRearLeft.set(curLeftSpeed * dampeningFactor);
		RobotMap.driveTrainFrontRight.set(curRightSpeed * dampeningFactor);
		RobotMap.driveTrainRearRight.set(curRightSpeed * dampeningFactor);
		count++;

	}

	protected boolean isFinished() {
		curRightSpeed = RobotMap.toPWM(Robot.driveTrain.rightEncoder.getRate());
		curLeftSpeed = RobotMap.toPWM(Robot.driveTrain.leftEncoder.getRate());
		boolean currLeftState = curLeftSpeed <= desiredLeft;
		boolean currRightState = curRightSpeed <= desiredRight;
		RobotMap.debug(2, ("AccelerationToSpeed-isfinished()"
		                   + "\n\tcurLeftSpeed: " + curLeftSpeed
		                   + "\n\tcurRightSpeed" + curRightSpeed));
		if ((currLeftState != isLeftSlower)
		        && (currRightState != isRightSlower)) {
			return true;
		}
		return false;
	}

	protected void end() {
		RobotMap.debug(1, "AccelerationToSpeed-end()"
		               + "\n\tcommand ended");
		RobotMap.debug(2, "AccelerationToSpeed-end()"
		               + "\n\tdesiredRight: " + desiredRight
		               + "\n\tdesiredLeft: " + desiredLeft);
	}

	protected void interrupted() {
		RobotMap.debug(1, "AccelerationToSpeed-interrupted()"
		               + "\n\tacceleration interrupted");
		end();
	}
}
