package org.usfirst.frc3501.RiceCatRobot.commands;

import org.usfirst.frc3501.RiceCatRobot.Robot;
import org.usfirst.frc3501.RiceCatRobot.RobotMap;

import edu.wpi.first.wpilibj.command.Command;

/**
 * 2nd Version of Accelerate which takes an actual inches per second
 *
 */
public class AccelerateToSpeed extends Command {
	private double desiredRight;
	private double desiredLeft;
	private boolean isRightSlower;
	private boolean isLeftSlower;
	private double curLeftSpeed;
	private double curRightSpeed;

	// debug constants
	private int count;

	/*
	 * @param This command requires left and right speeds that are inches/sec
	 */
	public AccelerateToSpeed(double leftSpeed, double rightSpeed) {
		requires(Robot.driveTrain);
		this.desiredLeft = leftSpeed;
		this.desiredRight = rightSpeed;
		this.isRightSlower = false;
		this.isLeftSlower = false;
		this.count = 0;
	}

	protected void initialize() {
		curLeftSpeed = Robot.driveTrain.leftEncoder.getRate();
		curRightSpeed = Robot.driveTrain.rightEncoder.getRate();
		isLeftSlower = curLeftSpeed < desiredLeft;
		isRightSlower = curRightSpeed < desiredRight;
		RobotMap.debug(1, ("accelerateToJoystickValue-initialize()"
		                   + "\n\tDesired Right Speed: " + desiredRight
		                   + "\n\tDesired Left Speed: " + desiredLeft
		                   + "\n\tCurrent Drive Left Speed: " + curLeftSpeed)
		               + "\n\tCurrent drive right speed" + curRightSpeed);

	}

	protected void execute() {
		if (Math.abs(desiredLeft - curLeftSpeed) > RobotMap.TOLERABLE_CHANGE_IN_SPEED) {
			// set the lastLeft to this value here
			// (sets it to a calibration ratio; false average)
			curLeftSpeed = ((desiredLeft + curLeftSpeed * 5) / 6);
		} else {
			// otherwise set lastLeft to the leftStick value
			curLeftSpeed = desiredLeft;
		}
		// do the same for the rightStick
		if (Math.abs(desiredRight - curRightSpeed) >
		        RobotMap.TOLERABLE_CHANGE_IN_SPEED) {
			curRightSpeed = ((desiredRight + curRightSpeed * 5) / 6);
		} else {
			curRightSpeed = desiredRight;
		}
		double PWMLeft = RobotMap.toPWM(curLeftSpeed);
		double PWMRight = RobotMap.toPWM(curRightSpeed);
		if (count % 5 == 0) {
			RobotMap.debug(2, ("accelerationToJoystickValue-execute()"
			                   + "\n\tCurrent Left Speed: " + curLeftSpeed
			                   + "\n\tCurrent Right Speed: " + curRightSpeed
			                   + "\n\tPWM Values: " + PWMLeft + " " + PWMRight));
		}
		RobotMap.driveTrainFrontLeft.set(PWMLeft);
		RobotMap.driveTrainRearLeft.set(PWMLeft);
		RobotMap.driveTrainFrontRight.set(PWMRight);
		RobotMap.driveTrainRearRight.set(PWMRight);
		count++;
	}

	protected boolean isFinished() {
		curLeftSpeed = Robot.driveTrain.leftEncoder.getRate();
		curRightSpeed = Robot.driveTrain.rightEncoder.getRate();
		boolean currLeftState = curLeftSpeed <= desiredLeft;
		boolean currRightState = curRightSpeed <= desiredRight;
		RobotMap.debug(2, ("accelerationtoJoystickValue-isFinished()\n"
		                   + "\tCurrent Left Speed: " + curLeftSpeed
		                   + "\n\tCurrent Right Speed" + curRightSpeed));
		if ((currLeftState != isLeftSlower)
		        && (currRightState != isRightSlower)) {
			return true;
		}
		return false;
	}

	protected void end() {
		RobotMap.debug(1, "accelerationToJoystickValue-end()"
		               + "\tcommand ended");
	}

	protected void interrupted() {
		RobotMap.debug(1, "accelerationToJoystickValue-interrupted()"
		               + "\tcommand interrupted");
		end();
	}
}
