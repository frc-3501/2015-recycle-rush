package org.usfirst.frc3501.RiceCatRobot.commands;

import org.usfirst.frc3501.RiceCatRobot.RobotMap;
import org.usfirst.frc3501.RiceCatRobot.RobotMap.Direction;

import edu.wpi.first.wpilibj.command.CommandGroup;

/**
 * Pre-conditions: Robot is facing an yellow tote and the auton zone.
 * Assumptions: Robot will drive straight & be able to push the object in front
 * of it & go over the scoring platform. The robot is facing a yellow tote and
 * will just drive forward and push it to the autozone.
 *
 * The robot is facing a yellow tote and will just drive forward and push it to
 * the autozone. formatting
 */
public class BrickBot extends CommandGroup {
	public static double moveDistance = 60;

	public BrickBot(boolean useDeadReckoning) {
		if (useDeadReckoning) {
			addSequential(new DriveFor(RobotMap.SECONDS_TO_AUTO_ZONE, Direction.FORWARD));
			// move for 5 seconds
		} else {
			addSequential(new DriveDistance(RobotMap.INCHES_TO_AUTO_ZONE));
			// move 60 inches
		}
	}
}
