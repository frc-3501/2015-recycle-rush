package org.usfirst.frc3501.RiceCatRobot.commands;

import org.usfirst.frc3501.RiceCatRobot.Robot;
import org.usfirst.frc3501.RiceCatRobot.RobotMap;

import edu.wpi.first.wpilibj.command.Command;
/*
 * Drives a certain distance in inches
 */
public class DriveDistance extends Command {
	private double desiredDistance = 0;
	private double initialDistance;
	// debug constants
	private int count;
	/*
	 * @param Distance must be in inches
	 */
	public DriveDistance(double distance) {
		requires(Robot.driveTrain);
		initialDistance = Robot.driveTrain.getAverageDistance();
		this.desiredDistance = distance + initialDistance;
		this.count = 0;
	}
	protected void initialize() {
		RobotMap.debug(2, "move-initialize()");
	}

	protected void execute() {
		double speed = RobotMap.speedAtDist(Math.abs(
		                                        Robot.driveTrain.getAverageDistance() - initialDistance),
		                                    Math.abs(desiredDistance));
		RobotMap.driveTrainFrontLeft.set(-Math.signum(
		                                     desiredDistance) * speed);
		RobotMap.driveTrainFrontRight.set(Math.signum(
		                                      desiredDistance) * speed);
		RobotMap.driveTrainRearLeft.set(-Math.signum(
		                                    desiredDistance) * speed);
		RobotMap.driveTrainRearRight.set(Math.signum(
		                                     desiredDistance) * speed);
//		if (Math.abs(Robot.driveTrain.getAverageDistance() - desiredDistance) < 12) {
//			System.out.println("we in");
//			RobotMap.driveTrainFrontLeft.set(-Math.signum(
//			                                     desiredDistance) * 0.2);
//			RobotMap.driveTrainFrontRight.set(Math.signum(
//			                                      desiredDistance) * 0.2);
//			RobotMap.driveTrainRearLeft.set(-Math.signum(
//			                                    desiredDistance) * 0.2);
//			RobotMap.driveTrainRearRight.set(Math.signum(
//			                                     desiredDistance) * 0.2);
//		} else if (Robot.driveTrain.getAverageDistance() > 12 + curDistance) {
//			System.out.println("We in 2.0");
//			RobotMap.driveTrainFrontLeft.set(-Math.signum(
//			                                     desiredDistance) * .5);
//			RobotMap.driveTrainFrontRight.set(Math.signum(
//			                                      desiredDistance) * .5);
//			RobotMap.driveTrainRearLeft.set(-Math.signum(
//			                                    desiredDistance) * .5);
//			RobotMap.driveTrainRearRight.set(Math.signum(
//			                                     desiredDistance) * .5);
//		} else {
//			System.out.println("We in");
//			RobotMap.driveTrainFrontLeft.set(-Math.signum(
//			                                     desiredDistance) * 0.2);
//			RobotMap.driveTrainFrontRight.set(Math.signum(
//			                                      desiredDistance) * 0.2);
//			RobotMap.driveTrainRearLeft.set(-Math.signum(
//			                                    desiredDistance) * 0.2);
//			RobotMap.driveTrainRearRight.set(Math.signum(
//			                                     desiredDistance) * 0.2);
//		}
		if (count % 5 == 0) {
			RobotMap.debug(
			    2,
			    ("move-execute()" + "\tAverage Distance: "
			     + Robot.driveTrain.getAverageDistance()
			     + "\n\tLeft Distance" + Robot.driveTrain.getLeftEncoderDistance())
			    + "\n\tRight Distance"
			    + Robot.driveTrain.getRightEncoderDistance());
		}
	}

	protected boolean isFinished() {
		return Math.abs(Robot.driveTrain.getAverageDistance()) >= Math.abs(
		           desiredDistance);
	}

	protected void end() {
		RobotMap.debug(1, "move-end()" + "\tcommand ended");
	}

	protected void interrupted() {
		RobotMap.debug(1, "move-interrupted()" + "\tmove command interrupted");
		end();
	}
}
