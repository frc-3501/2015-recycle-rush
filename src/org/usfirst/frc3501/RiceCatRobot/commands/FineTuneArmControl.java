package org.usfirst.frc3501.RiceCatRobot.commands;

import org.usfirst.frc3501.RiceCatRobot.OI;
import org.usfirst.frc3501.RiceCatRobot.Robot;
import org.usfirst.frc3501.RiceCatRobot.RobotMap;

import edu.wpi.first.wpilibj.command.Command;

/*
 * Uses POV or hat and fine tunes the arm.
 */
public class FineTuneArmControl extends Command {

	public FineTuneArmControl() {
		requires(Robot.arm);
	}

	protected void initialize() {
	}

	// Called repeatedly when this Command is scheduled to run
	protected void execute() {
		RobotMap.debug(1, "FineTuneArmControl-execute() called"
		               + "\n\tRobot.oi.getPOVValue() = " + Robot.oi.getPOVValue());
		Robot.arm.fineTuneControl(Robot.oi.getPOVValue());
	}

	protected boolean isFinished() {
		return Robot.oi.getPOVValue() == OI.POV_NOT_PRESSED;
	}

	// Called once after isFinished returns true
	protected void end() {
		Robot.arm.stopMoving();
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	// TODO: decide what needs to be done if this method is called twice in a
	// row or if it's interrupted by another command
	// such as move arm to level or move arm down.
	protected void interrupted() {
		end();
	}
}