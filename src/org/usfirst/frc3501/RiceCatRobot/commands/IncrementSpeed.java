package org.usfirst.frc3501.RiceCatRobot.commands;

import org.usfirst.frc3501.RiceCatRobot.Robot;

import edu.wpi.first.wpilibj.command.Command;

public class IncrementSpeed extends Command {
	int direction;
	public IncrementSpeed(int direction) {
		requires(Robot.claw);
		this.direction = direction;
	}
	@Override
	protected void initialize() {
	}

	@Override
	protected void execute() {
		Robot.claw.incrementSpeed(direction);
	}

	@Override
	protected boolean isFinished() {
		return true;
	}

	@Override
	protected void end() {
	}

	@Override
	protected void interrupted() {
	}

}
