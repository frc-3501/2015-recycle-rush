package org.usfirst.frc3501.RiceCatRobot.commands;

import org.usfirst.frc3501.RiceCatRobot.Robot;
import org.usfirst.frc3501.RiceCatRobot.RobotMap;

import edu.wpi.first.wpilibj.command.Command;

/**
 * Move the arm down one level
 *
 */
public class MoveArmDownLevel extends Command {
	public MoveArmDownLevel() {
		requires(Robot.arm);
	}

	// Called just before this Command runs the first time
	protected void initialize() {
	}

	// Called repeatedly when this Command is scheduled to run
	protected void execute() {
	}

	// Make this return true when this Command no longer needs to run execute()
	protected boolean isFinished() {
		// if arm lidar value is within 0.01 of targetValue
		return true;
	}

	// Called once after isFinished returns true
	protected void end() {
		Robot.arm.stopMoving();
		RobotMap.debug(3, "Arm moved down");
	}
	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	protected void interrupted() {
	}
}
