package org.usfirst.frc3501.RiceCatRobot.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc3501.RiceCatRobot.Robot;
import org.usfirst.frc3501.RiceCatRobot.RobotMap;
/**
 * Move arm up one level
 */
public class MoveArmUpLevel extends Command {
	private final double targetLidarValue = 0;

	public MoveArmUpLevel() {

		requires(Robot.arm);

	}

	// TODO add timer so if arm takes too long to move, stop command
	// TODO: what happens if button is pressed multiple times? - does initialize
	// run again when button is pressed multiple times
	// TODO: what happens if another command is called while this one is being
	// called(ex: pressed moveArmUpLevel then driver changed mind and pressed
	// moveArmDownLeve)
	// TODO consider merging moveArmDOwn and moveArmUp into one command
	// Called just before this Command runs the first time
	protected void initialize() {
		RobotMap.debug(1, "IN INIT MOVEARMUP");

		RobotMap.debug(1, "targetLidarValue: " + targetLidarValue);

		// move arm up at high speed
		Robot.arm.setArmSpeeds(RobotMap.ARM_HIGH_SPEED);
		RobotMap.debug(1, "Moving arm up");
	}

	// Called repeatedly when this Command is scheduled to run
	protected void execute() {
	}

	// Make this return true when this Command no longer needs to run execute()

	// TODO change < to <=
	// TODO: let isFinished return true after some time (if arm gets stuck or
	// something)
	protected boolean isFinished() {
		return true;
	}

	// Called once after isFinished returns true
	protected void end() {
		Robot.arm.stopMoving();
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	// TODO: decide what needs to be done if this method is called twice in a
	// row or if it's interrupted by another command
	// such as move arm to level or move arm down.
	@Override
	protected void interrupted() {
	}
}
