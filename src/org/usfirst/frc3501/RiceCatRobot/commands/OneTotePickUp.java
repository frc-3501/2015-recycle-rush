package org.usfirst.frc3501.RiceCatRobot.commands;

import org.usfirst.frc3501.RiceCatRobot.Robot;
import org.usfirst.frc3501.RiceCatRobot.RobotMap;
import org.usfirst.frc3501.RiceCatRobot.RobotMap.Direction;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.WaitCommand;

/**
 * Strategy: The robot will pick up one tote and deposit tote in auton zone.
 * preconditions: Robot is facing a yellow tote w/ auton zone on its left side.
 *
 */
public class OneTotePickUp extends CommandGroup {
	public static double moveDistance = 60;
	Timer timer;
	public OneTotePickUp(boolean isDeadReckoning) {
		timer.start();
		requires(Robot.driveTrain);
		requires(Robot.arm);
		addSequential(new CloseClaw()); // Close the Claw
		addSequential(new WaitCommand(1.0));
		if (isDeadReckoning) {
			System.out.println("In dead reckoning");
			addSequential(new MoveArmFor(RobotMap.SECONDS_UP_ONE_LEVEL, Direction.UP));
			addSequential(new WaitCommand(1.0));
			addSequential(new TurnFor(RobotMap.SECONDS_TO_TURN_90, Direction.LEFT));
			addSequential(new WaitCommand(1.0));
			// turn left(direction 0) 90 degrees
			addSequential(new DriveFor(RobotMap.SECONDS_TO_AUTO_ZONE, Direction.FORWARD));
			addSequential(new WaitCommand(1.0));
			addSequential(new OpenClaw());
		} else {
			addSequential(new MoveArmUpLevel());
			addSequential(new TurnDegrees(Direction.LEFT, 90));
			// turn left(direction 0) 90 degrees
			addSequential(new DriveDistance(RobotMap.INCHES_TO_AUTO_ZONE));
			addSequential(new OpenClaw());
		}
	}
}

