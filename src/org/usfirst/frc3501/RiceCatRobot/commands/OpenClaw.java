package org.usfirst.frc3501.RiceCatRobot.commands;

import edu.wpi.first.wpilibj.command.Command;

import org.usfirst.frc3501.RiceCatRobot.Robot;

/**
 * Opens claw by reversing solenoids.
 *
 */
public class OpenClaw extends Command {

	public OpenClaw() {
		requires(Robot.claw);
	}

	// Called just before this Command runs the first time
	protected void initialize() {
		System.out.println("IN INIT OPENCLAW");
	}

	// Called repeatedly when this Command is scheduled to run
	protected void execute() {
		Robot.claw.openClaw();
		System.out.println("Opening Claw");
	}

	// Make this return true when this Command no longer needs to run execute()
	protected boolean isFinished() {
		// ask double solenoid what its state is then return true if value is
		// kforward else
		System.out.println("Claw Opened");
		return Robot.claw.isOpen();
	}

	// Called once after isFinished returns true
	protected void end() {

	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	protected void interrupted() {
	}
}
