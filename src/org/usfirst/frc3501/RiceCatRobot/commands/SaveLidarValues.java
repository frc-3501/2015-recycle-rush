package org.usfirst.frc3501.RiceCatRobot.commands;

import edu.wpi.first.wpilibj.command.Command;

import org.usfirst.frc3501.RiceCatRobot.Robot;
import org.usfirst.frc3501.RiceCatRobot.RobotMap;

/**
 * This command uses a loop that hard sets certain sonar levels into 1D array.
 *
 */
public class SaveLidarValues extends Command {

	double[] sonarValuesForLevels = RobotMap.armLevelLidarValues;
	int arrayCounter;

	public SaveLidarValues() {
		// Use requires() here to declare subsystem dependencies
		// eg. requires(chassis);
		requires(Robot.arm);
	}

	// Called just before this Command runs the first time
	protected void initialize() {
		sonarValuesForLevels[0] = 0;
		arrayCounter = 1;
	}

	// Called repeatedly when this Command is scheduled to run
	protected void execute() {
		++arrayCounter;
	}

	// Make this return true when this Command no longer needs to run execute()
	protected boolean isFinished() {
		return false;
	}

	// Called once after isFinished returns true
	protected void end() {
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	protected void interrupted() {
	}
}
