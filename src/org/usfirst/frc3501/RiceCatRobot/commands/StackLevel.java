package org.usfirst.frc3501.RiceCatRobot.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class StackLevel extends CommandGroup {
	public StackLevel(int level) {
		addSequential(new MoveArmToLevel(level));
		addSequential(new OpenClaw());
		addSequential(new MoveArmToLevel(1));
		addSequential(new CloseClaw());
	}
}
