package org.usfirst.frc3501.RiceCatRobot.commands;

import org.usfirst.frc3501.RiceCatRobot.Robot;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class ToggleClaw extends Command {

	public ToggleClaw() {

	}

	// Called just before this Command runs the first time
	protected void initialize() {
		Robot.claw.toggleOn = !Robot.claw.toggleOn;
	}

	// Called repeatedly when this Command is scheduled to run
	protected void execute() {
		if (Robot.claw.toggleOn && Robot.claw.isOpen()) {
			Robot.claw.closeClaw();
		}
	}

	// Make this return true when this Command no longer needs to run execute()
	protected boolean isFinished() {
		return true;
	}

	// Called once after isFinished returns true
	protected void end() {

	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	protected void interrupted() {
	}
}
