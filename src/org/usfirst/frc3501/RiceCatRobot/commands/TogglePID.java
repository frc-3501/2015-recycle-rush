package org.usfirst.frc3501.RiceCatRobot.commands;

import org.usfirst.frc3501.RiceCatRobot.RobotMap;

import edu.wpi.first.wpilibj.command.Command;

public class TogglePID extends Command {

	@Override
	protected void initialize() {
	}

	@Override
	protected void execute() {
		RobotMap.driveWithPID = !RobotMap.driveWithPID;
	}

	@Override
	protected boolean isFinished() {
		return true;
	}

	@Override
	protected void end() {
	}

	@Override
	protected void interrupted() {
	}

}
