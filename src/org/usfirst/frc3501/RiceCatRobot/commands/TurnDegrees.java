package org.usfirst.frc3501.RiceCatRobot.commands;

import org.usfirst.frc3501.RiceCatRobot.Robot;
import org.usfirst.frc3501.RiceCatRobot.RobotMap;
import org.usfirst.frc3501.RiceCatRobot.RobotMap.Direction;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.command.Command;

/**
 * The method turns a certain degree left or right
 *
 */
public class TurnDegrees extends Command {

	/*
	 * direction is 0 or 1 which = left or right respectively
	 */
	private Encoder leftEncoder, rightEncoder;
	private Direction direction;
	private double targetDistance, radians;
	// debug constants
	private int count;

	public TurnDegrees(Direction direction, int degrees) {
		requires(Robot.driveTrain);
		this.radians = Math.PI * degrees / 180.0;
		this.leftEncoder = RobotMap.driveTrainLeftEncoder;
		this.rightEncoder = RobotMap.driveTrainRightEncoder;
		this.direction = direction;
		count = 0;
	}

	// Called just before this Command runs the first time
	protected void initialize() {
		targetDistance = RobotMap.DISTANCE_FROM_CENTER_TO_WHEELS * radians
		                 + Robot.driveTrain.getAverageDistance();
		Robot.driveTrain.resetEncoders();
	}

	// Called repeatedly when this Command is scheduled to run
	protected void execute() {
		if (direction == Direction.LEFT) {
			RobotMap.driveTrainFrontLeft.set(-0.5);
			RobotMap.driveTrainRearLeft.set(-0.5);
			RobotMap.driveTrainFrontRight.set(0.5);
			RobotMap.driveTrainRearRight.set(0.5);
		} else if (direction == Direction.RIGHT) {
			RobotMap.driveTrainFrontLeft.set(0.5);
			RobotMap.driveTrainRearLeft.set(0.5);
			RobotMap.driveTrainFrontRight.set(-0.5);
			RobotMap.driveTrainRearRight.set(-0.5);
		}
		if (count % 5 == 0) {
			RobotMap.debug(
			    2,
			    ("turn-execute()" + "\tAverage Distance: "
			     + Robot.driveTrain.getAverageDistance()
			     + "\n\tLeft Distance" + Robot.driveTrain
			     .getLeftEncoderDistance())
			    + "\n\tRight Distance"
			    + Robot.driveTrain.getRightEncoderDistance());
		}
	}

	protected boolean isFinished() {
		if (Math.abs(leftEncoder.getDistance()) >= Math.abs(targetDistance)
		        && Math.abs(rightEncoder.getDistance()) >= Math.abs(targetDistance)) {
			RobotMap.debug(
			    2,
			    ("turn-isFinished()" + "\tFinished Average Distance: "
			     + Robot.driveTrain.getAverageDistance()
			     + "\n\tFinished Left Distance" + Robot.driveTrain
			     .getLeftEncoderDistance())
			    + "\n\tFinished Right Distance"
			    + Robot.driveTrain.getRightEncoderDistance());
			return true;
		}
		return false;
	}

	// Called once after isFinished returns true
	protected void end() {
		RobotMap.debug(1, "turn-end()" + "\n\tMove ended");
	}

	// Called when another command which requires one or more of the same
	// subsystems is scheduled to run
	protected void interrupted() {
		RobotMap.debug(1, "turn-interrupted()" + "\n\tTurn interrupted");
		end();
	}
}
