package org.usfirst.frc3501.RiceCatRobot.commands;

import org.usfirst.frc3501.RiceCatRobot.RobotMap;

import edu.wpi.first.wpilibj.command.Command;

public class changeD extends Command {
	double change;

	public changeD(double increment) {
		change = increment;
	}

	@Override
	protected void initialize() {
	}

	@Override
	protected void execute() {
		RobotMap.D += change;
		if (RobotMap.D > 1) {
			RobotMap.D = 1;
		} else if (RobotMap.P < 0) {
			RobotMap.D = 0;
		}
		RobotMap.frontLeft.setPID(RobotMap.P, RobotMap.I, RobotMap.D);
		RobotMap.frontRight.setPID(RobotMap.P, RobotMap.I, RobotMap.D);
		RobotMap.rearRight.setPID(RobotMap.P, RobotMap.I, RobotMap.D);
		RobotMap.rearLeft.setPID(RobotMap.P, RobotMap.I, RobotMap.D);
	}

	@Override
	protected boolean isFinished() {
		return true;
	}

	@Override
	protected void end() {
	}

	@Override
	protected void interrupted() {
	}

}
