package org.usfirst.frc3501.RiceCatRobot.commands;

import org.usfirst.frc3501.RiceCatRobot.RobotMap;

import edu.wpi.first.wpilibj.command.Command;

public class changeI extends Command {
	double change;
	public changeI(double increment) {
		change = increment;
	}
	@Override
	protected void initialize() {
	}

	@Override
	protected void execute() {
		RobotMap.I = RobotMap.I + change;
		if (RobotMap.I > 1) {
			RobotMap.I = 1;
		} else if (RobotMap.P < 0) {
			RobotMap.I = 0;
		}
		RobotMap.frontLeft.setPID(RobotMap.P, RobotMap.I, RobotMap.D);
		RobotMap.frontRight.setPID(RobotMap.P, RobotMap.I, RobotMap.D);
		RobotMap.rearRight.setPID(RobotMap.P, RobotMap.I, RobotMap.D);
		RobotMap.rearLeft.setPID(RobotMap.P, RobotMap.I, RobotMap.D);
	}

	@Override
	protected boolean isFinished() {
		return true;
	}

	@Override
	protected void end() {
	}

	@Override
	protected void interrupted() {
	}

}
