package org.usfirst.frc3501.RiceCatRobot.commands;

import org.usfirst.frc3501.RiceCatRobot.RobotMap;
import edu.wpi.first.wpilibj.command.Command;

public class changeP extends Command {
	double change;
	public changeP(double increment) {
		change = increment;
		System.out.println("Change P initialized");
	}
	@Override
	protected void initialize() {
		System.out.println("Change P initialized for first time");
	}

	@Override
	protected void execute() {
		RobotMap.P = RobotMap.P + change;
		RobotMap.frontLeft.setPID(RobotMap.P, RobotMap.I, RobotMap.D);
		RobotMap.frontRight.setPID(RobotMap.P, RobotMap.I, RobotMap.D);
		RobotMap.rearRight.setPID(RobotMap.P, RobotMap.I, RobotMap.D);
		RobotMap.rearLeft.setPID(RobotMap.P, RobotMap.I, RobotMap.D);
	}

	@Override
	protected boolean isFinished() {
		return true;
	}

	@Override
	protected void end() {
	}

	@Override
	protected void interrupted() {
	}

}
