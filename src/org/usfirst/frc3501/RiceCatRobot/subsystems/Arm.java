package org.usfirst.frc3501.RiceCatRobot.subsystems;
import org.usfirst.frc3501.RiceCatRobot.RobotMap;

import edu.wpi.first.wpilibj.CANJaguar;
import edu.wpi.first.wpilibj.command.Subsystem;

public class Arm extends Subsystem {
	CANJaguar leftArm = RobotMap.armLeft, rightArm = RobotMap.armRight;

	public void initDefaultCommand() {

		// Set the default command for a subsystem here.
		// setDefaultCommand(new MySpecialCommand());
	}

	public void fineTuneControl(double d) {
		if(Math.abs(d) < 0.1){
			d = 0;
		}else if (d > 0) {
			d *= d;
		} else{
			d *= -d;
		}
		setArmSpeeds(d);
	}

	public void setArmSpeeds(final double speed) {
		leftArm.set(-speed);
		rightArm.set(speed);
	}

	public void stopMoving() {
		leftArm.set(0);
		rightArm.set(0);
	}

}