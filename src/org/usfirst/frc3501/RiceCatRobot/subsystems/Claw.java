package org.usfirst.frc3501.RiceCatRobot.subsystems;

import org.usfirst.frc3501.RiceCatRobot.RobotMap;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.command.Subsystem;

public class Claw extends Subsystem {

	DoubleSolenoid solenoid = RobotMap.clawSolenoid;
	public double currentSpeed = 0.3;
	public boolean isOpen = true; 
	public boolean toggleOn=false;

	public void initDefaultCommand() {

	}

	public void closeClaw() {
		solenoid.set(RobotMap.close);
		isOpen = false;
	}

	public void openClaw() {
		solenoid.set(RobotMap.open);
		isOpen = true;
	}

	public boolean isOpen() {
		return solenoid.get()==RobotMap.open;
	}

	public void incrementSpeed(int direction) {
		currentSpeed += (direction) * 0.1;
		if (currentSpeed > 1) {
			currentSpeed = 1;
		}
		if (currentSpeed < 0) {
			currentSpeed = 0;
		}
	}
}
