package org.usfirst.frc3501.RiceCatRobot.subsystems;

import org.usfirst.frc3501.RiceCatRobot.Robot;
import org.usfirst.frc3501.RiceCatRobot.RobotMap;
import edu.wpi.first.wpilibj.CANJaguar;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.command.Subsystem;

public class DriveTrain extends Subsystem {
	public static double DRIVE_TRAIN_DEAD_ZONE = 0.1;
	public CANJaguar frontLeft = RobotMap.driveTrainFrontLeft,
	                 frontRight = RobotMap.driveTrainFrontRight,
	                 rearLeft = RobotMap.driveTrainRearLeft,
	                 rearRight = RobotMap.driveTrainRearRight;
	public Encoder leftEncoder = RobotMap.driveTrainLeftEncoder,
	               rightEncoder = RobotMap.driveTrainRightEncoder;
	double previousLeftDriveSpeed = 0;
	double previousRightDriveSpeed = 0;
	static final double tolerableChangeInJoystickValue =
	    RobotMap.TOLERABLE_CHANGE_IN_JOYSTICK_PWM;
	static final double DAMPENING_FACTOR = RobotMap.DAMPENING_FACTOR;

	public void initDefaultCommand() {

	}

	public DriveTrain() {
	}

	public double getLeftEncoderRaw() {
		return leftEncoder.getRaw();
	}
	public double getRightEncoderRaw() {
		return rightEncoder.getRaw();
	}

	public void resetEncoders() {
		leftEncoder.reset();
		rightEncoder.reset();
	}

	public double getRightEncoderSpeed() {
		// Returns in per second
		return rightEncoder.getRate();
	}

	public double getLeftEncoderSpeed() {
		return leftEncoder.getRate();
	}

	public double getAverageSpeed() {
		return (getLeftEncoderSpeed() + getRightEncoderSpeed()) / 2.0;
	}

	public double getRightEncoderDistance() {
		// Returns distance in in
		return rightEncoder.getDistance();
	}

	public double getLeftEncoderDistance() {
		// Returns distance in in
		return leftEncoder.getDistance();
	}

	public double getAverageDistance() {
		return (getLeftEncoderDistance() + getRightEncoderDistance()) / 2.0;
	}

	public double getCurrentSpeed() {
		return (leftEncoder.getRate() + rightEncoder.getRate()) / 2;
	}

	public void setMotorSpeeds(double leftSpeed, double rightSpeed) {
		if (Math.abs(leftSpeed) < DRIVE_TRAIN_DEAD_ZONE) {
			leftSpeed = 0;
		}
		if (Math.abs(rightSpeed) < DRIVE_TRAIN_DEAD_ZONE) {
			rightSpeed = 0;
		}
		this.frontLeft.set(leftSpeed);
		this.frontRight.set(-rightSpeed);
		this.rearLeft.set(leftSpeed);
		this.rearRight.set(-rightSpeed);
	}

	public void drive() {
		RobotMap.frontLeft.setSetpoint(Robot.oi.leftJoystick.getY());
		RobotMap.frontRight.setSetpoint(-Robot.oi.rightJoystick.getY());
		RobotMap.rearLeft.setSetpoint(Robot.oi.leftJoystick.getY());
		RobotMap.rearRight.setSetpoint(-Robot.oi.rightJoystick.getY());
	}
	public void arcadeDrive(double yVal, double twist) {
		if (Math.abs(yVal) < DRIVE_TRAIN_DEAD_ZONE) {
			yVal = 0;
		}
		if (Math.abs(twist) < DRIVE_TRAIN_DEAD_ZONE) {
			twist = 0;
		}
		double leftMotorSpeed;
		double rightMotorSpeed;
		// adjust the sensitivity (yVal+rootof (yval)) / 2
		yVal = (yVal + Math.signum(yVal) * Math.sqrt(Math.abs(yVal))) / 2;
		// adjust the sensitivity (twist+rootof (twist)) / 2
		twist = (twist + Math.signum(twist) * Math.sqrt(Math.abs(twist))) / 2;
		if (yVal > 0.0) {
			if (twist > 0.0) {
				leftMotorSpeed = yVal - twist;
				rightMotorSpeed = Math.max(yVal, twist);
			} else {
				leftMotorSpeed = Math.max(yVal, -twist);
				rightMotorSpeed = yVal + twist;
			}
		} else {
			if (twist > 0.0) {
				leftMotorSpeed = -Math.max(-yVal, twist);
				rightMotorSpeed = yVal + twist;
			} else {
				leftMotorSpeed = yVal - twist;
				rightMotorSpeed = -Math.max(-yVal, -twist);
			}
		}
		setMotorSpeeds(leftMotorSpeed, rightMotorSpeed);
	}

	public void setSpeed() {
		/*
		 * Algorithm does basic weighting average to prevent drastic changes in
		 * motor values
		 */

		if (Math.abs(Robot.oi.leftJoystick.getY() - previousLeftDriveSpeed) >
		        tolerableChangeInJoystickValue) {
			// set the lastLeft to this value here
			// (sets it to a calibration ratio; false average)
			previousLeftDriveSpeed = ((Robot.oi.leftJoystick.getY() +
			                           previousLeftDriveSpeed * 5) / 6);
		} else {
			// otherwise set lastLeft to the leftStick value
			previousLeftDriveSpeed = Robot.oi.leftJoystick.getY();
		}
		if (Math.abs(Robot.oi.rightJoystick.getY() - previousRightDriveSpeed) >
		        tolerableChangeInJoystickValue) {
			// set the lastLeft to this value here
			// (sets it to a calibration ratio; false average)
			previousRightDriveSpeed = ((Robot.oi.rightJoystick.getY() +
			                            previousRightDriveSpeed * 5) / 6);
		} else {
			// otherwise set lastLeft to the leftStick value
			previousRightDriveSpeed = Robot.oi.rightJoystick.getY();
		}
		setMotorSpeeds(previousLeftDriveSpeed, previousRightDriveSpeed);
	}
}
